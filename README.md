# aimms

Assignment for the role of Senior Software Engineer JavaScript at AIMMS.

## Description

Aimms package collects information about a content in a given path - directories and files.

### Init

As I was building a nodeJS module, I've started with a

```
npm init
npm install jasmine --save-dev
npm install eslint --save-dev
```

I've added jasmine dev-dependency, as I wanted to use TDD and the assignment has requirements to write unit tests. I've chosed jasmine, because it is a test framework and includes a tiny test runner inside.
I've also added eslint with airbnb linting style, because I'm familiar with it.
Then, I've added a few commands to `package.json` to ease running tools.

```
"scripts": {
  "test": "jasmine",
  "lint": "eslint"
},
```

I've decided to use ES2015 in a project, as a requirement was to use Node >= 4.4.7, and it supports [more then 50% ES2015](http://node.green/).
```
describe('aimms', () => {
  it('package exists', () => {
    ...
```
So, [first step is done](https://bitbucket.org/korzio/aimms/commits/ead3c8fb2291fcc45c127354a2562fef47b5e7c7) - environment is ready.

### Basic functional

Basic module which I use in a test work is [node fs](https://nodejs.org/dist/v4.4.7/docs/api/fs.html#fs_file_system). Also I need a [path node module](https://nodejs.org/dist/latest-v4.x/docs/api/path.html#path_path_dirname_p) to normalize results.
I've decided to use [mock-fs](https://github.com/tschaub/mock-fs) package, which provides mocks for `node fs module`, so can be easily used in testing process.

The code in `lib/index.js` is splitted into 2 functions - `aimms`, which is an entry point, and a `collect` function, which iterates though directories and calls recursively itself, if a sub-directory has been found.

Basic functional is done with [a second step](https://bitbucket.org/korzio/aimms/commits/4160ff78a82966f93b2294172a5b63c296589b2c).

### Optimization

When I've added an example from the assignment description to test package, I've found that it needs to expose filenames before going deep into directories. So, I [had to refactor](https://bitbucket.org/korzio/aimms/commits/322aeb71b86bb201ea45cee675cc18169fd928b6) a bit `collect` function to iterate through local files first:
```
...
  .filter((fullPath) => {
    ...
    if (stats.isFile(path)) {
      filenames.push(fullPath);
      return false;
    }
    return stats.isDirectory(path);
  })
```

After that I've decided to add a real filesystem example for the current package:

```
  it('aimms module internal files and folders', () => {
    const expected = jasmine.objectContaining({
      dirnames: jasmine.arrayContaining(['node_modules']),
      filenames: jasmine.arrayContaining([
        'lib/index.js',
        'spec/fs.spec.js',
        'spec/index.spec.js',
      ]),
    });
    ...
```

In fact, I've added this test also for the purpose to check the overall time, on which tests run. So, the result first was near `0.34 seconds`.
I knew, [that it could be better](https://reaktor.com/blog/javascript-performance-fundamentals-make-bluebird-fast/). Easy steps to optimize are:

- Minimize passing arguments - didn't change overall execution time

- Minimize recursive `collect` calls.

```
const collect = (localPath, dirnames, filenames) => {
  dirnames.push(localPath);
  const content = fs.readdirSync(localPath);
  if (!content.length) {
    return;
  }
```
I could try to move the check into caller function. So, if a dir doesn't have internal files, the function will not be called. I believe, this will not be a big optimization - at least because there is not a big amount of empty directories in common case.

- [Google closure compiler](https://closure-compiler.appspot.com/) optimization. I've added a `index.min.js` which was a compiled and `Advanced` optimized version of `index.js` (actually, I had to manually fix some issues with compiled code). And result speed became almost `20%` faster - `0.289 seconds`. So, after investigating a bit I've found, that the biggest speed optimization happens because of imported objects and used functions.
```
const join = require('path').join;
...
const readdirSync = fs.readdirSync;
```
After [update](https://bitbucket.org/korzio/aimms/commits/0293fa9664e5ced526d62de1d236b9fa979535d0), the overall time decreased to `0.287 seconds`.

- I've also had a thought, that I could cache results. In a way to check the `mtime - Modified Time` or `ctime - Change Time` of `fs.statSync` method applied to internal directories. I decided to leave it for now, as it is a bit specific task to solve (and I'm not sure that it was meant in initial requirements).

### Promisify

Next step was to add `async` possibility to existing functional because of the requirement

> Assume that this component will be used as part of a larger application that is also performing other tasks (e.g. a web service).

I didn't want to use callbacks from native Node.js functions, because it could consequently lead to messy code.
I've decided to use [Promises](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise) because Node.js has [almost full support](http://node.green/#Promise) from the version `4.3.2` and all that I wanted to use besides basic functionality was `Promise.all`.
For that purpose, I've created a file `promisify` which intent was to wrap basic functions (`fs.exists`, `fs.readdir`, `fs.stat`) into Promises.

- **promisify** - wrapper for callback-input functions
- **withError**, **withoutError** - factories, which can handle different types of callbacks.

> The callback gets two arguments (err, files)... (**fs.readdir**)

`fs.exists` despite of other functions has a [custom callback structure](https://nodejs.org/dist/latest-v4.x/docs/api/fs.html#fs_fs_exists_path_callback)

> Then call the callback argument with either true or false. (**fs.exists**)


So, I've updated existing tests to handle promise-based functionaliy.
After that I refactored the existing code to support testing examples. I didn't change the algorithm itself, just added promises logic.

#### Small changes

- Moved `jasmine` configuration to `package.json`
- Moved `collect` functionality to separate file

#### Possible further optimizations

- Reduce promise.all to use consequence instead
- Optimize **withError**, **withoutError** factories - cache internal functions, not to generate new one each time

## Install

```
npm install
```

## Test

```
npm test
npm run lint
```

## References

- [jasmine](http://jasmine.github.io/edge/introduction.html)
- [eslint](http://eslint.org/)
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [node ES2015 support](http://node.green/)
- [Three JavaScript performance fundamentals that make Bluebird fast](https://reaktor.com/blog/javascript-performance-fundamentals-make-bluebird-fast/)
- [Google closure compiler](https://closure-compiler.appspot.com/)
- [Promise](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise)
