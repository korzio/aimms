const promisifyUtils = require('./promisify');

const promisify = promisifyUtils.promisify;
const withError = promisifyUtils.withError;
const withoutError = promisifyUtils.withoutError;

describe('promisify utils', () => {
  describe('promisify() transforms function with callback to', () => {
    it('rejected promise if wrong callback was specified', (done) => {
      const promiseFunction = promisify(() => ({}));
      promiseFunction()
        .catch(done);
    });

    it('promise if callback was specified', (done) => {
      const promiseFunction = promisify(() => ({}), resolve => resolve());
      promiseFunction()
        .then(done);
    });

    it('promise if reject callback was specified', (done) => {
      const promiseFunction = promisify(() => ({}), (resolve, reject) => reject());
      promiseFunction()
        .catch(done);
    });
  });

  describe('withError() generates a function which', () => {
    it('rejects on first argument', (done) => {
      withError(null, done)(true);
    });

    it('resolves on second argument', (done) => {
      withError(done)();
    });
  });

  describe('withoutError() generates a function which', () => {
    it('resolves on any argument', () => {
      const spyWithoutArguments = jasmine.createSpy('spy');
      withoutError(spyWithoutArguments)();
      expect(spyWithoutArguments).toHaveBeenCalled();
      const spyWithArguments = jasmine.createSpy('spy');
      withoutError(spyWithArguments)(true);
      expect(spyWithArguments).toHaveBeenCalled();
    });
  });
});
