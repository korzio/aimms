/* eslint-disable prefer-spread, prefer-rest-params */
/**
 * @module aimms
 * @name promisify
 * @description
 * Promise utilities for async functions
 */

/**
 * @function
 * @name promisify
 * @description
 * Transforms asynchronous functions so they returns Promises
 *
 * @param {Function} fn - function to transform
 * @param {Function} callbackFactory - which callback strategy should be applied
 * @return {Function}
 */
const promisify = (fn, callbackFactory) => function promisifyTransform() {
  const args = Array.prototype.slice.call(arguments);
  return new Promise((resolve, reject) => {
    fn.apply(null, args.concat(callbackFactory(resolve, reject)));
  });
};

/**
 * @function
 * @name withError
 * @description
 * Factory function, which rejects promise with existing first argument, resolves otherwise
 *
 * @param {Function} resolve
 * @param {Function} reject
 * @return {Function}
 */
const withError =
  (resolve, reject) =>
    (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    };

/**
 * @function
 * @name withoutError
 * @description
 * Factory function, which always resolves promise
 *
 * @param {Function} resolve
 * @return {Function}
 */
const withoutError =
  resolve =>
    result => resolve(result);

module.exports = {
  promisify,
  withError,
  withoutError,
};
