/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const mock = require('mock-fs');
const aimms = require('../');

describe('collect exposes', () => {
  afterEach(() => {
    mock.restore();
  });

  it('empty structure on wrong path', (done) => {
    mock({
      path: {
        'file.txt': '',
      },
    });

    const expected = {
      filenames: [],
      dirnames: [],
    };

    aimms('path/file.txt')
      .then((res) => {
        expect(res).toEqual(expected);
        return aimms('path/wrong/path');
      })
      .then((res) => {
        expect(res).toEqual(expected);
      })
      .then(done);
  });

  it('file and dir from a given path', (done) => {
    mock({
      path: {
        'file.txt': '',
        dir: {},
      },
    });

    const expected = {
      filenames: [
        'path/file.txt',
      ],
      dirnames: [
        'path',
        'path/dir',
      ],
    };

    aimms('path/')
      .then((res) => {
        expect(res).toEqual(expected);
      })
      .then(done);
  });

  it('files and dirs from assignment example', (done) => {
    mock({
      foo: {
        bar: {
          'bar1.txt': '',
          'bar2.txt': '',
          baz: {},
        },
        'f1.txt': '',
        'f2.txt': '',
      },
    });

    const expected = {
      filenames: [
        'foo/f1.txt',
        'foo/f2.txt',
        'foo/bar/bar1.txt',
        'foo/bar/bar2.txt',
      ],
      dirnames: [
        'foo',
        'foo/bar',
        'foo/bar/baz',
      ],
    };

    aimms('foo/')
      .then((res) => {
        expect(res).toEqual(expected);
      })
      .then(done);
  });

  it('aimms module internal files and folders', (done) => {
    const expected = jasmine.objectContaining({
      dirnames: jasmine.arrayContaining(['node_modules']),
      filenames: jasmine.arrayContaining([
        'lib/index.js',
      ]),
    });

    aimms('.')
      .then((res) => {
        expect(res).toEqual(expected);
      })
      .then(done);
  });
});
