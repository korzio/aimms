const aimms = require('../');

describe('aimms', () => {
  it('package exists', () => {
    expect(aimms).toEqual(jasmine.any(Function));
  });

  it('returns promise with json structure with filenames and dirnames', (done) => {
    const expected = {
      filenames: [],
      dirnames: [],
    };

    aimms()
      .then((results) => {
        expect(results).toEqual(expected);
      })
      .then(done);
  });
});
