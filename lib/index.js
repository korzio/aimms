/**
 * @module aimms
 * @name aimms
 * @description
 * Aimms package collects information about a content in a given path - directories and files.
 *
 * @requires fs
 * @requires collect
 * @requires promisify
 */

const fs = require('fs');

const collect = require('./collect');
const promisifyUtils = require('./promisify');

const promisify = promisifyUtils.promisify;
const withError = promisifyUtils.withError;
const withoutError = promisifyUtils.withoutError;

const stat = promisify(fs.stat, withError);
const exists = promisify(fs.exists, withoutError);

/**
 * @function
 * @name aimms
 * @description
 * Entry point for the package aimms
 *
 * @param {String} inputPath
 * @returns {ContentsStructure}
 */
const aimms = (inputPath) => {
  /**
   * @typedef {Object} ContentsStructure
   * @property {Array} filenames
   * @property {Array} dirnames
   */
  const collection = {
    filenames: [],
    dirnames: [],
  };

  // check that inputPath exists and it is a dir
  return exists(inputPath)
    .then((pathExists) => {
      if (!pathExists) {
        return collection;
      }

      return stat(inputPath)
        .then((stats) => {
          if (!stats.isDirectory()) {
            return collection;
          }

          const normalPath = inputPath.replace(/\/+$/, '');
          return collect(normalPath, collection)
            .then(() => collection);
        });
    })
    .catch(() => collection);
};

module.exports = aimms;
