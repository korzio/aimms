/* eslint-disable consistent-return */
/**
 * @module aimms
 * @name collect
 * @description
 * Aimms package collect functional.
 *
 * @requires fs
 * @requires path
 */

const fs = require('fs');
const join = require('path').join;
const promisifyUtils = require('./promisify');

const promisify = promisifyUtils.promisify;
const withError = promisifyUtils.withError;

const readdir = promisify(fs.readdir, withError);
const stat = promisify(fs.stat, withError);

/**
 * @function
 * @name collect
 * @description
 * Collect function checks the information about a given path.
 * If inner item is a directory - recursively calls itself.
 *
 * @param {String} localPath - path for directory to look in
 * @param {ContentsStructure} collection
 * @return {Promise}
 */
const collect = (localPath, collection) => {
  const dirnames = collection.dirnames;
  const filenames = collection.filenames;

  dirnames.push(localPath);
  return readdir(localPath)
    .then(content =>
      Promise.all(
        content
          .map(contentPath => join(localPath, contentPath))
          .map(fullPath => stat(fullPath)
            .then((stats) => {
              if (stats.isDirectory()) {
                return collect(fullPath, collection);
              }

              if (stats.isFile()) {
                filenames.push(fullPath);
              }
            })
          )
      )
    );
};

module.exports = collect;
